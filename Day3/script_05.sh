#!/bin/bash

# to check leap year or not
clear

echo -n "Enter year "
read year

# year must be divisible by 4 && should not be century year
# after every 400 years there is century year which is leap

if [ `expr $year % 4` -eq 0 -a `expr $year % 100` -ne 0 -o `expr $year % 400` -eq 0 ]
then 
    echo "$year is leap year"
else 
    echo "$year is not leap year"
fi