#!/bin/bash

# 7. Write a Program to find whether a given number is positive or negative 

clear 

echo -n "Enter number : "
read num

if [ $num -gt -1 ]
then
    echo -n "Number $num is positive "
else
    echo -n "Number $num is negative "

fi

exit
