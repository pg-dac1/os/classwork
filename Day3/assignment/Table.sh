#!/bin/bash

# Write a program to print the table of a given number. 

# echo -n "Enter number : "
# read num

i=1

for i in {1..10..1}
do
    echo `expr $1 \* $i`
done