#!/bin/bash

# Write a shell script to display menu like “1. Date, 2. Cal, 3. Ls, 4. Pwd, 5. Exit” and
# execute the commands depending on user choice. 

clear



while [ true ]
do
    echo "1.Date"
    echo "2.Calender"
    echo "3.Ls"
    echo "4.Pwd"
    echo "0.Exit"

    read choice

    case $choice in 
    1)  echo -n "Date is :"
        date +"%d/%m/%Y"
    ;;
    
    2)  echo -n "Calender is :"
        cal
    ;;

    3)  echo -n "Content of directory :"
        ls -l 
    ;;

    4)  echo -n "Absolute path of current directory : "
        pwd
    ;;

    0)  echo -n "exit"
        exit
    ;;


    esac

done