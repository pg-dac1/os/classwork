#!/bin/bash

#. Write a program to find given number of terms in the Fibonacci series. 

echo -n "Enter a number : "
read num
i=1
n1=1
n2=0
n3=0

while [ $i -le $num ]
do
    n3=`expr $n1 + $n2`
    echo -n "$n3 "
    n1=$n2
    n2=$n3
    i=`expr $i + 1`
done
