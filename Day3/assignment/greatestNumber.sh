#!/bin/bash

# 5. Write a Program to find the greatest of three numbers 

clear

echo -n "enter n1 : "
read n1

echo -n "enter n2 : "
read n2

echo -n "enter n3 : "
read n3

greatNum=$n1

if [ $greatNum -lt $n2 ]
then
    greatNum=$n2    
fi 

if [ $greatNum -lt $n3 ]
then
    greatNum=$n3
fi
    

echo "Greatest number among is : $greatNum"

exit