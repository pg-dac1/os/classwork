#!/bin/bash

# Write a program to calculate gross salary if the DA is 40%,
#  HRA is 20% of basic salary.
# Accept basic salary form user and 
# display gross salary (Result can be floating point
# value). 

clear 

echo -n "Enter basic salary : "

read basicSal

grossSal=`echo "$basicSal" | bc `
grossSal=`echo "$grossSal / 100" | bc`

echo -n "Gross salary is : $grossSal"

exit