#!/bin/bash

# Write a shell script to accept the name from the user and check whether user entered
# name is file or directory. If name is file display its size and if it is directory display its
# contents.

clear 

echo -n "Enter file name : "
read filepath

if [ -e $filepath ] #check if  is valid 
then    

    if [ -f $filepath ] #check if  is valid 
    then    
        # cat $filepath #display files contents\
        filesize=$(stat -c %s "$filepath")
        echo "Size of $filepath = $filesize bytes."

    elif [ -d $filepath ] #check if  is directory
    then    
        # cat $filepath #display files contents\
        echo "Contents of directory"    
        ls -l $filepath    
    fi

else 
    echo "Invalid Filepath"    
fi


# menuDriven.sh
