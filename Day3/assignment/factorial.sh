#!/bin/bash

# Write a program to find the factorial of given number.

echo -n "Enter a number : "
read num

i=$num
product=1
while [ $i -gt 0 ]
do
    product=`expr $i \* $product`
    i=`expr $i - 1`
done

echo -n "Factorial of number $num : $product"
