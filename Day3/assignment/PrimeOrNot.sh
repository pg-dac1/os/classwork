#!/bin/bash

clear

echo -n "Enter a number : "
read num
i=2

while [ $i -ne $num ]
do
    res=`echo "$num % $i" | bc` 

    if [ $res -eq 0 ]
    then
        echo "$num Is not prime number"
        exit
    fi
    i=`expr $i + 1`
done

echo "$num Is a prime number"

exit