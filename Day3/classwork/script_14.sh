#!/bin/bash

# script to implement menu driven program of basic 4 function calculator

function addition( )
{
    res=`expr $1 + $2`
    echo "$res"
}

function substraction( )
{
    res=`expr $1 - $2`
    echo "$res"
}

function multiplication( )
{
    res=`expr $1 \* $2`
    echo "$res"
}

function division( )
{
    res=`expr $1 / $2`
    echo "$res"
}


while [ true ]
do
    clear

    # display menu list
    echo "***** basic calculator *****"
    echo "0. exit"
    echo "1. addition"
    echo "2. substraction"
    echo "3. multiplication"
    echo "4. division"

    # accept choice from the user
    echo -n "enter the choice : "
    read choice

    if [ $choice -eq 0 ]
    then
        exit
    fi

    echo -n "enter operand-1 : "
    read op1

    echo -n "enter operand-2 : "
    read op2

    case $choice in
    1)
        res=$(addition $op1 $op2)
        echo "result is : $res"
        ;;  # break of case label-1

    2)
        res=$(substraction $op1 $op2)
        echo "result is : $res"
        ;;  # break of case label-2

    3)
        res=$(multiplication $op1 $op2)
        echo "result is : $res"
        ;;  # break of case label-3

    4)
        res=$(division $op1 $op2)
        echo "result is : $res"
        ;;  # break of case label-4
    *)
        echo "invalid operation"
        ;;  # break of case label-1
    esac

    echo "press enter to continue ..."
    read enter
    
done

