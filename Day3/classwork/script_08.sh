#!/bin/bash

# script to display executable files only in cur dir

echo "name of an executable files in a cur dir are : "

for filename in `ls`
do
    if [ -e $filename -a -x $filename ]
    then
        echo "$filename"
    fi
done

# output of ls command will be filenames in a  cur dir i.e. collection of strings
# in above for loop in each iteration value of filename will be taken from collection
# file1.txt
# file3.txt
# script_01.sh
# script_02.sh
# script_03.sh
# script_04.sh
# script_05.sh
# script_06.sh
# script_07.sh
# script_08.sh

exit



