#!/bin/bash

# script to check wheather user entered year is a leap year or not

clear

# accept year from user
echo -n "enter the year : "
read yr

# year must be completely divisible by 4 && it should not be a century year, exeception is that
# after every 400 years there is a century year which is a leap year.
# if( yr % 4 == 0 && yr % 100 != 0 || yr % 400 == 0 ) then year is a leap year

if [ `expr $yr % 4` -eq 0 -a `expr $yr % 100` -ne 0 -o `expr $yr % 400` -eq 0 ]
then
    echo "$yr is a leap year"
else
    echo "$yr is not a leap year"
fi

exit


