#!/bin/bash

# script to calculate area of rectangle

clear

# accept length from user
echo -n "enter the length: "
read len

# accept breadth from user
echo -n "enter the breadth : "
read br

# calculate by formula => area of rectangle = len * br

area=`expr $len \* $br`
echo "area of rectangle is : $area sq. units"

exit



