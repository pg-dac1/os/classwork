#!/bin/bash

# if cur active shell program is bash then to mention name of shebang line is not mandaory, but 
# if cur any other shell program is active and if we want to execute script by using bash shell then
# then it is mandaory to mention shebang line.

# script to do addition of two numbers:

clear

# accept the value of n1 from user
echo -n "enter the value of n1 : "
read n1 # read command is used to scan the value of any type of variable

# accept the value of n2 from user
echo -n "enter the value of n2 : "
read n2

# to addition operation between n1 & n2 and store it into result var
result=`expr $n1 + $n2`

# display result
echo "result is : $result"

exit







