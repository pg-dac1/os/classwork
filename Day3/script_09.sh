#!/bin/bash

# script to print pattern

# accept no of rows
echo -n "enter rows "
read n

i=0

while [ $i -lt $n ] #outer while
do
    j=0

    while [ $j -lt `expr $n - $i` ]
    do
    echo -n " * "
    j=`expr $j + 1`
    done

    echo ""
    i=`expr $i + 1`
done