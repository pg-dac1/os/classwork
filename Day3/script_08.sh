#!/bin/bash

# script to display executable files only in a current directory
for filename in `ls`
do
    if [ -e $filename -a -x $filename ]
    then 
    echo "$filename"
    fi
done
# out put of ls command is string i.e filenmes of current directory
# in above for loop in each iteration value of filename will be taken from collection