#!/bin/bash

# acept file path from user

# check filepath valid or invalid
# if invalid display msg invalid filepath
# if valid 
    # if regular file then display its contents by "CAT command"

    # if dir then display its contents by "ls command"

    # otherwise display filepath is not regular not directory file

clear

# accept filepath from user
echo "Enter filepath "
read filepath

if [ -e $filepath ] #if valid 
then 
    if [ -f $filepath ] #if filepath is regular file
    then
    echo "$filepath is regular file contents are : "
    cat $filepath

    elif [ -d  $filepath ]
    then
    echo "$filepath is directory file contents are : "
    ls -l $filepath

    else
    echo "$filepath is neither regular file nor directory file"
    fi


else
    echo "$filepath is invalid filepath"    
fi
# if else ladder

