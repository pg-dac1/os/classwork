#!/bin/bash

clear
# to display table of number while loop
echo -n "enter the number"

read num

i=1 #initilization of counter

echo "table of number $num "
while [ $i -le 10 ]
do 
    res=`expr $num \* $i`
    echo "$res"
    i=`expr $i + 1`
done

# to display table of number until loop
echo "for loop"

echo "table of number $num "
# for i in 1 2 3 4 5 6 7 8 9 10
for i in {1..10..2}
do 
    res=`expr $num \* $i`
    echo "$res"
done