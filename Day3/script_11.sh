#!/bin/bash

# script to do sum of all positional parameters

#initially sum is 0
sum=0

echo "no. of pos params are : $#"

for num in $*
do
    # in each iteration add each param in a sum sequentially
    sum=`expr $sum + $num`
done

# in above for loop an output of $* => collection of all pos params, in each iteration
# one value will be taken sequentially and will be added into the sum 

# display final sum
echo "sum of all pos params is : $sum"
exit