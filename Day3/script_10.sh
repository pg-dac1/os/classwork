#!/bin/bash

# script to do additon of two position params

clear

# if positional params are not = 2
if [ $# -ne 2 ]
then
    echo "invalid no. of pos params to script : $0"
    exit #script will be exited
fi

sum=`expr $1 + $2`
echo "sum is : $sum"

exit