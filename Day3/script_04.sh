#!/bin/bash

clear

# define PI as a constant
PI=3.14

# accept radius
echo -n "Enter radius "
read radius

area=`echo "$PI * $radius * $radius" | bc`
echo "area of circle is : $area "